
# ETR

Playing with UICollectionCompositionalLayout
This repository is a bit big because images are included in the assets folder

Features:
- Sections with side scrolling items
- Collapsible header
- Entity with data structured by section
- Programmatic UI
