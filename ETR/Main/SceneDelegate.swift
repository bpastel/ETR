//
//  SceneDelegate.swift
//  ETR
//
//  Created by rose on 31/01/2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = (scene as? UIWindowScene) {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = HomeViewController()
            self.window = window
            window.makeKeyAndVisible()
        }
    }
}

