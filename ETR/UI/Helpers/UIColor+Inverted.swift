//
//  UIColor+Inverted.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

extension UIColor {
    static let invertedBackground = UIColor { traits in
        systemBackground.resolvedColor(with: traits.invertedStyle())
    }
    static let invertedLabel = UIColor { traits in
        label.resolvedColor(with: traits.invertedStyle())
    }
}

enum ColorStyle {
    case standard, inverted
}
