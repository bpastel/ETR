//
//  UIStackView+Helpers.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ views: UIView...) {
        views.forEach { view in addArrangedSubview(view) }
    }
}
