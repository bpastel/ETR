//
//  UIImage+Helpers.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

extension UIImage {
    convenience init?(named name: String?) {
        guard let name = name else { return nil }
        self.init(named: name)
    }
}
