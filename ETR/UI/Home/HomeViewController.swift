//
//  HomeViewController.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

class HomeViewController: UIViewController {
    private lazy var contentView: HomeView = .init()
    private var statusBarStyle: UIStatusBarStyle = .lightContent
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }
    
    override func loadView() {
        view = contentView
        contentView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateWithContent()
    }
    
    private func updateWithContent() {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Content>()
        snapshot.appendSections(Section.allCases)
        Section.allCases.forEach {
            snapshot.appendItems($0.stubData(), toSection: $0)
        }
        contentView.apply(snapshot)
    }
}

extension HomeViewController: HomeViewDelegate {
    func updateStatusBarStyle(to style: UIStatusBarStyle) {
        if statusBarStyle == .lightContent && traitCollection.userInterfaceStyle == .dark {return}
        statusBarStyle = style
        UIView.animate(withDuration: 0.4) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
}
