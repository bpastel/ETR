import UIKit

protocol HeaderViewDelegate: AnyObject {
    func updateStatusBarStyle(to style: UIStatusBarStyle)
}

class HeaderView: UIView {
    weak var delegate: HeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private var statusBarStyle: UIStatusBarStyle = .lightContent { didSet {delegate?.updateStatusBarStyle(to: statusBarStyle)} }
    
    private let card = UIView()
    private let imageView = UIImageView()
    private let searchContainer = UIView()
    private let titleLabel = UITextView()
    private let separator = UIView()
    
    private var searchBar = UIButton()
    private var button = UIButton()
    
    private lazy var minHeight : CGFloat = {44 + 12 + 12 + safeAreaInsets.top}()
    private var maxHeight: CGFloat = 480
    private var maxTopSpace: CGFloat = 40
    private var heightConstraint = NSLayoutConstraint()
    private var topSpaceConstraint = NSLayoutConstraint()
    
    func configure() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .black
        card.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        card.layer.cornerRadius = 20
        card.layer.masksToBounds = true
        
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "background")
        
        searchContainer.backgroundColor = .systemBackground
        searchContainer.setBackgroundAlpha(0)
        
        var searchBarConfig = UIButton.Configuration.filled()
        var attributeContainerForButton = AttributeContainer()
        attributeContainerForButton.font = .custom(style: .button)
        searchBarConfig.attributedTitle = AttributedString("Commencez votre recherche", attributes: attributeContainerForButton)
        searchBarConfig.image = UIImage(systemName: "magnifyingglass")
        searchBarConfig.imagePlacement = .leading
        searchBarConfig.imagePadding = 8
        searchBarConfig.preferredSymbolConfigurationForImage = UIImage.SymbolConfiguration(pointSize: UIFont.custom(style: .button).pointSize)
        searchBarConfig.cornerStyle = .capsule
        searchBarConfig.baseBackgroundColor = .secondarySystemBackground
        searchBarConfig.baseForegroundColor = .label
        
        searchBar = UIButton(configuration: searchBarConfig, primaryAction: nil)
        
        titleLabel.text = "Vous ne savez pas\noù partir ?\nParfait."
        titleLabel.textColor = .white
        titleLabel.font = .custom(style: .largeTitle)
        titleLabel.setLineHeightMultiple(to: 0.9)
        titleLabel.isScrollEnabled = false
        titleLabel.isEditable = false
        titleLabel.isSelectable = false
        titleLabel.backgroundColor = .clear
        
        var buttonConfig = UIButton.Configuration.filled()
        var attributeContainerForTitle = AttributeContainer()
        attributeContainerForTitle.font = .custom(style: .button)
        buttonConfig.attributedTitle = AttributedString("Trouver un hôte", attributes: attributeContainerForTitle)
        buttonConfig.baseBackgroundColor = .secondarySystemBackground
        buttonConfig.cornerStyle = .medium
        buttonConfig.baseForegroundColor = .label
        buttonConfig.contentInsets = .init(top: 8, leading: 8, bottom: 8, trailing: 8)
        
        button = UIButton(configuration: buttonConfig, primaryAction: nil)

        separator.backgroundColor = .quaternaryLabel
    }
    
    func constrain() {
        addSubviews(card, titleLabel, button, separator)
        card.addSubviews(imageView, searchContainer)
        searchContainer.addSubviews(searchBar)
        
        card.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        searchContainer.translatesAutoresizingMaskIntoConstraints = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        heightConstraint = heightAnchor.constraint(equalToConstant: maxHeight)
        topSpaceConstraint = card.topAnchor.constraint(equalTo: topAnchor, constant: 40)

        NSLayoutConstraint.activate([
            heightConstraint,
            topSpaceConstraint,
            
            card.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            card.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            card.bottomAnchor
                .constraint(equalTo: bottomAnchor),
            
            imageView.leadingAnchor
                .constraint(equalTo: card.leadingAnchor),
            imageView.trailingAnchor
                .constraint(equalTo: card.trailingAnchor),
            imageView.topAnchor
                .constraint(equalTo: card.topAnchor),
            imageView.bottomAnchor
                .constraint(equalTo: card.bottomAnchor),
            
            searchContainer.topAnchor
                .constraint(equalTo: card.topAnchor),
            searchContainer.leadingAnchor
                .constraint(equalTo: card.leadingAnchor),
            searchContainer.trailingAnchor
                .constraint(equalTo: card.trailingAnchor),
            searchContainer.heightAnchor
                .constraint(greaterThanOrEqualToConstant: 60),
            
            searchBar.heightAnchor
                .constraint(equalToConstant: 44),
            searchBar.topAnchor
                .constraint(greaterThanOrEqualTo: searchContainer.topAnchor, constant: 24),
            searchBar.topAnchor
                .constraint(greaterThanOrEqualTo: safeAreaLayoutGuide.topAnchor, constant: 12),
            searchBar.leadingAnchor
                .constraint(equalTo: searchContainer.leadingAnchor, constant: 24),
            searchBar.trailingAnchor
                .constraint(equalTo: searchContainer.trailingAnchor, constant: -24),
            searchBar.bottomAnchor
                .constraint(equalTo: searchContainer.bottomAnchor, constant: -12),
            
            titleLabel.topAnchor
                .constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 160),
            titleLabel.leadingAnchor
                .constraint(equalTo: leadingAnchor, constant: 24),
            
            button.topAnchor
                .constraint(equalTo: titleLabel.bottomAnchor),
            button.leadingAnchor
                .constraint(equalTo: titleLabel.leadingAnchor),
            
            separator.heightAnchor
                .constraint(equalToConstant: 1),
            separator.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            separator.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            separator.bottomAnchor
                .constraint(equalTo: bottomAnchor)
        ])
    }
    
    override func safeAreaInsetsDidChange() {
        maxTopSpace = 40 + safeAreaInsets.top
        topSpaceConstraint.constant = maxTopSpace
    }
}

extension HeaderView {
    private var currentOffset: CGFloat {
        get {heightConstraint.constant}
        set {animate(to: newValue)}
    }
    
    func updateHeader(newY: CGFloat, oldY: CGFloat) -> CGFloat {
        let delta = newY - oldY
        let isMovingUp = delta > 0
        let isInContent = newY > 0
        let hasRoomToCollapse = currentOffset > minHeight
        let shouldCollapse = isMovingUp && isInContent && hasRoomToCollapse
        
        let isMovingDown = delta < 0
        let isBeyondContent = newY < 0
        let hasRoomToExpand = currentOffset < maxHeight
        let shouldExpand = isMovingDown && isBeyondContent && hasRoomToExpand
        
        if shouldCollapse || shouldExpand {
            currentOffset -= delta
            return newY - delta
        }
        return newY
    }
    
    private func animate(to value: CGFloat) {
        let clamped = max(min(value, maxHeight), minHeight)
        heightConstraint.constant = clamped
        let normalized = (value - minHeight) / (maxHeight - minHeight)
        switch normalized {
        case ..<0.5:
            animateToFifty(normalized)
        default:
            animateToOneHundred(normalized)
        }
    }
    
    private func animateToFifty(_ normalized: CGFloat) {
        let newTop = normalized * 2 * maxTopSpace
        topSpaceConstraint.constant = newTop
        searchContainer.setBackgroundAlpha(1 - normalized * 2)
        if newTop < 24 && statusBarStyle != .darkContent {
            statusBarStyle = .darkContent
        } else if newTop > 24 {
            statusBarStyle = .lightContent
        }
    }

    private func animateToOneHundred(_ normalized: CGFloat) {
        topSpaceConstraint.constant = maxTopSpace
        searchContainer.setBackgroundAlpha(0)
    }
}
