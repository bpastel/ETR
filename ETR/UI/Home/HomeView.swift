//
//  HomeView.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

protocol HomeViewDelegate: AnyObject {
    func updateStatusBarStyle(to style: UIStatusBarStyle)
}

class HomeView: UIView {
    
    weak var delegate: HomeViewDelegate?

    private let headerView = HeaderView()
    private lazy var collectionView = makeCollectionView()
    private lazy var dataSource = makeDataSource()
    
    private var oldYOffset: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configure() {
        collectionView.backgroundColor = .systemBackground
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        
        headerView.delegate = self
    }
    
    
    private func constrain() {
        addSubviews(headerView, collectionView)
        
        NSLayoutConstraint.activate([
            headerView.topAnchor
                .constraint(equalTo: topAnchor),
            headerView.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            headerView.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            headerView.bottomAnchor
                .constraint(equalTo: collectionView.topAnchor),

            collectionView.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            collectionView.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            collectionView.bottomAnchor
                .constraint(equalTo: bottomAnchor)
        ])
    }
    
    func apply(_ snapshot: NSDiffableDataSourceSnapshot<Section, Content>) {
        dataSource.apply(snapshot)
    }
}

extension HomeView {
    func makeCollectionView() -> UICollectionView {
        let layout = UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment in
            let section = Section.allCases[sectionIndex]
            switch section {
            case .nearby:
                return .sideScrollingTwoItem()
            case .experiences:
                return .invertedSideScrollingOneItem()
            case .info:
                return .footer()
            default:
                return .sideScrollingOneItem()
            }
        }
        layout.registerBackgrounds()
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    func makeDataSource() -> UICollectionViewDiffableDataSource<Section, Content> {
        let registrationSmallSquareCell = SmallSquareCell.registration()
        let registrationLargeSquareCell = LargeSquareCell.registration()
        let registrationInvertedLargeSquareCell = InvertedLargeSquareCell.registration()
        let registrationFooterCell = FooterCell.registration() { indexPath in indexPath.item % 4 != 3 }
        let dataSource = UICollectionViewDiffableDataSource<Section, Content> (collectionView: collectionView) { view, indexPath, item in
            let section = Section.allCases[indexPath.section]
            switch section {
            case .nearby: return view.dequeueConfiguredReusableCell(using: registrationSmallSquareCell, for: indexPath, item: item)
            case .experiences: return view.dequeueConfiguredReusableCell(using: registrationInvertedLargeSquareCell, for: indexPath, item: item)
            case .info: return view.dequeueConfiguredReusableCell(using: registrationFooterCell, for: indexPath, item: item)
            default: return view.dequeueConfiguredReusableCell(using: registrationLargeSquareCell, for: indexPath, item: item)
            }
        }
        let headers = Section.allCases.map {$0.headerContent}
        let headerRegistration = SectionHeader.registration(headers: headers)
        let invertedHeaderRegistration = InvertedHeader.registration(headers: headers)
        dataSource.supplementaryViewProvider = { collectionView, string, indexPath in
            let section = Section.allCases[indexPath.section]
            switch section {
            case .experiences: return collectionView.dequeueConfiguredReusableSupplementary(using: invertedHeaderRegistration, for: indexPath)
            default: return collectionView.dequeueConfiguredReusableSupplementary(using: headerRegistration, for: indexPath)
            }
        }
        return dataSource
    }
}

extension HomeView: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        let updatedY = headerView.updateHeader(newY: yOffset, oldY: oldYOffset)
        scrollView.contentOffset.y = updatedY
        oldYOffset = scrollView.contentOffset.y
    }
}

extension HomeView: HeaderViewDelegate {
    func updateStatusBarStyle(to style: UIStatusBarStyle) {
        delegate?.updateStatusBarStyle(to: style)
    }
}
