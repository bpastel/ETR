//
//  LargeSquareCell.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//
import UIKit

typealias LargeSquareCell = ContentCell<LargeSquareView>

class LargeSquareView: UIView, ContentConfiguringView {
    
    private lazy var style: ColorStyle = provideStyle()
    private let mainStack = UIStackView()
    private let imageView = UIImageView()
    private let labelStack = UIStackView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        mainStack.axis = .vertical
        mainStack.spacing = 10
        
        imageView.backgroundColor = .secondarySystemFill
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        labelStack.axis = .vertical
        labelStack.spacing = 2
        
        let textColor: UIColor = style == .inverted ? .invertedLabel : .label
        titleLabel.font = .custom(style: .headline)
        titleLabel.textColor = textColor
        subtitleLabel.font = .custom(style: .subheadline)
        subtitleLabel.textColor = textColor
    }
    
    func constrain() {
        addSubviews(mainStack)
        mainStack.addArrangedSubviews(imageView, labelStack)
        labelStack.addArrangedSubviews(titleLabel, subtitleLabel)
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            mainStack.topAnchor
                .constraint(equalTo: topAnchor),
            mainStack.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            mainStack.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            mainStack.bottomAnchor
                .constraint(equalTo: bottomAnchor),
            
            imageView.widthAnchor
                .constraint(equalTo: imageView.heightAnchor)
        ])
    }
    
    func configure(with content: Content?) {
        titleLabel.text = content?.title
        subtitleLabel.text = content?.subtitle
        imageView.image = UIImage(named: content?.image)
    }
    
    func provideStyle() -> ColorStyle {.standard}
}

typealias InvertedLargeSquareCell = ContentCell<InvertedLargeSquareView>

class InvertedLargeSquareView: LargeSquareView {
    override func provideStyle() -> ColorStyle {.inverted}
}
