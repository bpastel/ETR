//
//  ContentCell.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

protocol ContentConfiguringView: UIView {
    func configure(with content: Content?)
}

protocol SeparatorShowing: AnyObject {
    func showSeparator(_ shouldShow: Bool)
}

extension ContentCell: SeparatorShowing where View: SeparatorShowing {
    func showSeparator(_ shouldShow: Bool) {
        view.showSeparator(shouldShow)
    }
}

final class ContentCell<View: ContentConfiguringView>: UICollectionViewCell {
    private lazy var view: View = .init()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        constrain()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        constrain()
    }
    
    private func constrain() {
        contentView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor
                .constraint(equalTo: contentView.topAnchor),
            view.leadingAnchor
                .constraint(equalTo: contentView.leadingAnchor),
            view.trailingAnchor
                .constraint(equalTo: contentView.trailingAnchor),
            view.bottomAnchor
                .constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    func configure(with content: Content?) {
        view.configure(with: content)
    }
    
    static func registration(showSeparator: @escaping (IndexPath) -> Bool = {_ in false}) -> UICollectionView.CellRegistration<ContentCell<View>, Content> {
        UICollectionView.CellRegistration {cell, indexPath, content in
            cell.configure(with:content)
            if let cell = cell as? SeparatorShowing {
                let shouldShow = showSeparator(indexPath)
                cell.showSeparator(shouldShow)
            }
        }
    }
}
