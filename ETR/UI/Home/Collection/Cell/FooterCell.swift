//
//  FooterCell.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//


import UIKit

typealias FooterCell = ContentCell<FooterView>

final class FooterView: UIView, ContentConfiguringView {
    private let stack = UIStackView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let separator = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func configure() {
        stack.axis = .vertical
        stack.spacing = 4
        titleLabel.font = .custom(style: .headline)
        subtitleLabel.font = .custom(style: .subheadline)
        separator.backgroundColor = .quaternaryLabel
    }
    
    func constrain() {
        addSubviews(stack, separator)
        stack.addArrangedSubviews(titleLabel, subtitleLabel)
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            stack.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            stack.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            stack.topAnchor
                .constraint(equalTo: topAnchor, constant: 28),
            stack.bottomAnchor
                .constraint(equalTo: bottomAnchor, constant: -28),
            
            separator.topAnchor
                .constraint(equalTo: bottomAnchor),
            separator.leadingAnchor
                .constraint(equalTo: stack.leadingAnchor),
            separator.trailingAnchor
                .constraint(equalTo: stack.trailingAnchor),
            separator.heightAnchor
                .constraint(equalToConstant: 1)
        ])
    }
    
    func configure(with content: Content?) {
        titleLabel.text = content?.title
        subtitleLabel.text = content?.subtitle
        switch content?.style {
        case .standard: titleLabel.font = .custom(style: .headline)
        case .title: titleLabel.font = .custom(style: .title4)
        case .none: break
        }
    }
}

extension FooterView: SeparatorShowing {
    func showSeparator(_ shouldShow: Bool) {
        separator.alpha = shouldShow ? 0.8 : 0
    }
}
