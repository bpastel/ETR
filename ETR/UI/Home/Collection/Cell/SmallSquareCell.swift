//
//  SmallSquareCell.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

typealias SmallSquareCell = ContentCell<SmallSquareView> 

final class SmallSquareView: UIView, ContentConfiguringView {
    
    private let imageView = UIImageView()
    private let stack = UIStackView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        imageView.backgroundColor = .secondarySystemFill
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true

        stack.axis = .vertical
        stack.spacing = 8
        
        titleLabel.font = .custom(style: .headline)
        subtitleLabel.font = .custom(style: .subheadline)
    }

    func constrain() {
        addSubviews(imageView, stack)
        stack.addArrangedSubviews(titleLabel, subtitleLabel)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor
                .constraint(equalTo: topAnchor),
            imageView.bottomAnchor
                .constraint(equalTo: bottomAnchor),
            imageView.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            imageView.widthAnchor
                .constraint(equalTo: imageView.heightAnchor),
            
            stack.leadingAnchor
                .constraint(equalTo: imageView.trailingAnchor, constant: 10),
            stack.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            stack.centerYAnchor
                .constraint(equalTo: centerYAnchor)
        ])
    }
    
    func configure(with content: Content?) {
        titleLabel.text = content?.title
        subtitleLabel.text = content?.subtitle
        imageView.image = UIImage(named: content?.image) // using our UIImage initializer
    }
}

