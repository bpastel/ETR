//
//  SectionHeader.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

typealias SectionHeader = ContentHeader<SectionHeaderView>
typealias InvertedHeader = ContentHeader<InvertedHeaderView>

class SectionHeaderView: UIView, ContentConfiguringHeader {
    private lazy var style: ColorStyle = provideStyle()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
        constrain()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func configure() {
        directionalLayoutMargins = .init(top: 24, leading: 0, bottom: 0, trailing: 0)
        let textColor: UIColor = style == .inverted ? .invertedLabel : .label
        titleLabel.font = .custom(style: .title2)
        titleLabel.adjustsFontForContentSizeCategory = true
        titleLabel.numberOfLines = 0
        titleLabel.textColor = textColor
        subtitleLabel.font = .custom(style: .title4)
        subtitleLabel.numberOfLines = 0
        subtitleLabel.textColor = textColor
    }
    
    func constrain() {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.axis = .vertical
        stackView.spacing = 4
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.topAnchor
                .constraint(equalTo: layoutMarginsGuide.topAnchor),
            stackView.leadingAnchor
                .constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            stackView.trailingAnchor
                .constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            stackView.bottomAnchor
                .constraint(equalTo: layoutMarginsGuide.bottomAnchor)
        ])
    }
    
    func configure(with content: Content?) {
        titleLabel.text = content?.title
        subtitleLabel.text = content?.subtitle
    }
    
    
    func provideStyle() -> ColorStyle {.standard}
}

final class InvertedHeaderView: SectionHeaderView {
    override func provideStyle() -> ColorStyle {
        .inverted
    }
}

extension NSCollectionLayoutBoundarySupplementaryItem {
    convenience init(layoutSize: NSCollectionLayoutSize,
                     kind: UICollectionView.ElementKind,
                     alignment: NSRectAlignment) {
        self.init(layoutSize: layoutSize,
                  elementKind: kind.rawValue,
                  alignment: alignment)
    }

    static func header(layoutSize: NSCollectionLayoutSize) ->
    NSCollectionLayoutBoundarySupplementaryItem {
        .init(layoutSize: layoutSize, kind: .header, alignment: .top)
    }
}
