//
//  SectionHeader.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import UIKit

protocol ContentConfiguringHeader: UIView {
    func configure(with content: Content?)
}

class ContentHeader<View: ContentConfiguringHeader>: UICollectionReusableView {
    private lazy var view: View = .init()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        constrain()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        constrain()
    }
    
    private func constrain() {
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.topAnchor
                .constraint(equalTo: topAnchor),
            view.leadingAnchor
                .constraint(equalTo: leadingAnchor),
            view.trailingAnchor
                .constraint(equalTo: trailingAnchor),
            view.bottomAnchor
                .constraint(equalTo: bottomAnchor)
        ])
    }
    
    func configure(with content: Content?) {
        view.configure(with: content)
    }
    
    static func registration(headers: [Content?]) -> UICollectionView.SupplementaryRegistration<ContentHeader<View>> {
        UICollectionView.SupplementaryRegistration(kind: .header) {header, string, indexPath in
            let content = headers[indexPath.section]
            header.configure(with: content)
        }
    }
}

extension UICollectionView {
    enum ElementKind: String {
        case header
    }
}

extension UICollectionView.SupplementaryRegistration {
    init(kind: UICollectionView.ElementKind, handler: @escaping UICollectionView.SupplementaryRegistration<Supplementary>.Handler) {
        self.init(elementKind: kind.rawValue, handler: handler)
    }
}
