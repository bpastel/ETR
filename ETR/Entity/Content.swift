//
//  Content.swift
//  EasyTravel
//
//  Created by rose on 29/01/2022.
//

import Foundation

enum Section: Int, Hashable, CaseIterable {
    case nearby
    case stays
    case experiences
    case hosting
    case info
}

struct Content: Hashable {
    enum Style {
        case standard
        case title
    }
    let title: String
    let subtitle: String?
    let image: String?
    let style: Style
    
    init(title: String, subtitle: String?, image: String?, style: Style = .standard) {
        self.title = title
        self.subtitle = subtitle
        self.image = image
        self.style = style
    }
}

extension Section {
    var headerContent: Content? {
        switch self {
        case .nearby: return nil
        case .stays: return .init(title: "Séjour", subtitle: "Trouver un espace à vous", image: nil)
        case .experiences: return .init(title: "Expériences", subtitle: "Des activités uniques à vivre pendant votre voyage.", image: nil)
        case .hosting: return .init(title: "Hôtes", subtitle: "Des questions sur l'accueil des voyageurs ?", image: nil)
        case .info: return .init(title: "Informations générales", subtitle: nil, image: nil)
        }
    }
}

extension Section {
    func stubData() -> [Content] {
        switch self {
        case .nearby:
            return [
                .init(title: "Paris", subtitle: "1h30 de route", image: "paris"),
                .init(title: "Gap", subtitle: "2h30 de route", image: "gap"),
                .init(title: "Barcelonnette", subtitle: "3h de route", image: "barcelonnette"),
                .init(title: "Nice", subtitle: "2h de route", image: "nice"),
                .init(title: "Tokyo", subtitle: "11h de vol", image: "tokyo"),
                .init(title: "Fukuoka", subtitle: "12h30 de vol", image: "fukuoka"),
                .init(title: "Nagoya", subtitle: "11h30 de vol", image: "nagoya"),
                .init(title: "Nago", subtitle: "13h30 de vol", image: "nago")
            ]
        case .stays:
            return [
                .init(title: "Maison entière", subtitle: nil, image: "maisons"),
                .init(title: "Cabanes", subtitle: nil, image: "cabanes"),
                .init(title: "Lieux insolites", subtitle: nil, image: "insolite"),
                .init(title: "Animaux de compagnie bienvenus", subtitle: nil, image: "animaux"),
            ]
        case .experiences:
            return [
                .init(title: "Expériences en ligne",
                      subtitle: "Des expériences à vivre de chez vous",
                      image: "online-experiences"),
                .init(title: "Ateliers",
                      subtitle: "Ateliers ludiques pour toute la famille",
                      image: "ateliers"),
                .init(title: "Aventures",
                      subtitle: "Activités inoubliables par des habitants",
                      image: "aventures"),
            ]
        case .hosting:
            return [
                .init(title: "Accueillir chez vous", subtitle: nil, image: "hote1"),
                .init(title: "Proposer un atelier", subtitle: nil, image: "hote2"),
                .init(title: "Offrir une expérience", subtitle: nil, image: "hote3"),
            ]
        case .info:
            return [
                .init(title: "Assistance", subtitle: nil, image: nil, style: .title),
                .init(title: "Nos mesures face au Covid-19", subtitle: "Informations de sécurité", image: nil),
                .init(title: "Options d'annulation", subtitle: "Politique relative à l'annulation", image: nil),
                .init(title: "Centre d'aide", subtitle: "Signaler un problème de voisinage", image: nil),

                .init(title: "Communauté", subtitle: nil, image: nil, style: .title),
                .init(title: "ONG", subtitle: "Soutenir une initiative", image: nil),
                .init(title: "Lutte contre la discrimination", subtitle: "Découvrir les objectifs", image: nil),
                .init(title: "La diversité et l'intégration", subtitle: "Comment contribuer", image: nil),

                .init(title: "Accueil de voyageurs", subtitle: nil, image: nil, style: .title),
                .init(title: "Devenir hôte", subtitle: "Guide pratique", image: nil),
                .init(title: "Communauté", subtitle: "Forum de la communauté", image: nil),
                .init(title: "Ressources pour les hôtes", subtitle: "Accueillir des voyageurs", image: nil),

                .init(title: "À propos", subtitle: nil, image: nil, style: .title),
                .init(title: "Newsroom", subtitle: "Dernières nouvelles", image: nil),
                .init(title: "Carrières", subtitle: "Découvrire les opportunités", image: nil),
                .init(title: "En savoir plus sur les nouveautés", subtitle: "Ressources presse", image: nil),
            ]
        }
    }
}
